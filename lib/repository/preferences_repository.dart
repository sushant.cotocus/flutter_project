import 'package:cocktail_adventure/models/cocktail.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreferencesRepository {

  Future<void> saveCocktail(List<Cocktail> cocktails) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> _listCocktail = [];
    for (var element in cocktails) {
      _listCocktail.add(element.toJson());
    }
    prefs.setStringList('key', _listCocktail);
  }

  Future<List<Cocktail>> loadFavorite() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String>? prefsList = prefs.getStringList('key');
    List<Cocktail> finalList = [];
    prefsList?.forEach((element) {
      if (element.isNotEmpty) {
        finalList.add(Cocktail.fromJsonPreferences(element));
      }
    });

    return finalList;
  }

}