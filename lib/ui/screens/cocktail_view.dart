import 'package:cached_network_image/cached_network_image.dart';
import 'package:cocktail_adventure/blocs/cocktail_cubit.dart';
import 'package:cocktail_adventure/models/cocktail.dart';
import 'package:cocktail_adventure/repository/repository.dart';
import 'package:favorite_button/favorite_button.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class CocktailView extends StatefulWidget {
  const CocktailView({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _CocktailView();
}

class _CocktailView extends State<CocktailView> {


@override
  Widget build(BuildContext context) {
    return FutureBuilder<Cocktail>(
      future: Provider.of<Repository>(context, listen: false).loadRandom(),
      builder: (context, snapshot) {
        if(!snapshot.hasData) {
          return Column(
            children: const [
              CircularProgressIndicator(),
              Text("Loading...")
            ],
          );
        }
        Cocktail cocktail = snapshot.data!;
        List<Cocktail> favorites = Provider.of<CocktailCubit>(context, listen:false).state;
        return Column(
          children: [
            CachedNetworkImage(
              imageUrl: cocktail.imgUrl ?? '',
              placeholder: (context, url) => const CircularProgressIndicator(),
              errorWidget: (context, url, error) => const Icon(Icons.error),
            ),
            Column(
              children: [
                ListTile(
                  title: Row(
                    children: [
                      Text(
                      cocktail.name ?? 'Unknown',
                        style: GoogleFonts.dancingScript(
                            fontSize: 24,
                            fontWeight: FontWeight.bold
                        ),
                      ),
                      FavoriteButton(
                          isFavorite: favorites.contains(cocktail),
                          valueChanged: (_isFavorite) {
                            if(_isFavorite) {
                              Provider.of<CocktailCubit>(context, listen: false).addFavorite(cocktail);
                            } else {
                              // Provider.of<CocktailListCubit>(context, listen: false).saveFavorites(cocktail);
                            }
                          }
                      )
                    ]
                  ),
                  subtitle: Text(cocktail.instructionsEn ?? 'Unknown'),
                ),
                const Text('Ingredients :'),
                Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: cocktail.ingredients.length,
                    itemBuilder:(BuildContext context, int index) {
                      String? key = cocktail.ingredients.keys.elementAt(index);
                      return Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: Row(
                          children: [
                            Text('$key : '),
                            Text('${cocktail.ingredients[key]}'),
                          ],
                        ),
                      );
                    }
                  ),
                ),
              ]
            )
          ],
        );
      }
    );
  }
}
