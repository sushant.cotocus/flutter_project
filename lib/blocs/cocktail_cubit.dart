import 'package:bloc/bloc.dart';
import 'package:cocktail_adventure/models/cocktail.dart';
import 'package:cocktail_adventure/repository/repository.dart';

class CocktailCubit extends Cubit<List<Cocktail>> {
  CocktailCubit(this._repository) : super([]);
  final Repository _repository;
  void addFavorite(Cocktail cocktail) {
    emit([...state, cocktail]);
    _repository.saveFavorite(state);
  }

  void saveFavorites(List<Cocktail> cocktails) {
    _repository.saveFavorite(cocktails);
  }
  Future<void> loadFavorites() async {
    final List<Cocktail> cocktails = await _repository.loadFavorites();
    emit(cocktails);
  }

  Future<List<Cocktail>> searchCocktailByName(String letter) {
    return _repository.searchCocktailByName(letter);
  }
}